# Dockerfile to create usable image of drec application (only app no db)
FROM centos:centos8 as build

RUN dnf -y install git python38 python3-virtualenv

RUN virtualenv -p python3.8 drec
COPY drec/* drec/.
RUN drec/bin/pip install -r drec/requirements.txt

FROM centos:centos8
RUN dnf -y install python38
RUN su -c "useradd -ms /bin/bash drec"
WORKDIR /app
RUN su -c "chown -R drec:drec /app"
RUN su -c "chmod 755 /app"
USER drec

COPY --from=build /drec .

CMD [ "/app/bin/python", "drec.py" ]
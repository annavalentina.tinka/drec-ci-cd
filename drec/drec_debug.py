from cryptofeed import FeedHandler

from loadConf import load_config, str2list, validate_config
from crossfireT import *

from sys import exit


### Debug importing
from cryptofeed.backends.mongo import BookDeltaMongo, BookMongo, TradeMongo, TickerMongo
from cryptofeed.defines import BOOK_DELTA, L2_BOOK, TRADES, TICKER
from cryptofeed.exchanges import Coinbase


def main(exchange, max_depth, data_chanels, pairs, db_callbacks):
    """
    Because periods cannot be in keys in documents in mongo, the bids and asks dictionaries
    are converted to BSON. They will need to be decoded after being read
    """
    f = FeedHandler()
### Puvodni funkce    
#    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=db_callbacks))

### Predani callbacks naprimo parametrizovano
#    callbacks_debug2 = {"trades": TradeMongo(db_name, collection='trades')} - NEVYTVORI COLLECTION WTF
#    callbacks_debug = {TRADES: TradeMongo(db_name, collection='trades')}    - FUNGUJE, OBE PROMENE STEJNE
#
#    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=callbacks_debug))


### Prima definice funkce na generovani callbacks v tomto souboru - FUNGUJE
#    def get_callbacks_dict(callback_list, db_name, collection_dict):
#        callback_dict = dict()
#        for callback in callback_list:
#            if collection_dict.get(callback):
#                callback_dict.update({ conf2obj(callback, DATA_CHANELS) : conf2obj(callback, DB_CALLBACKS)(db_name, collection=collection_dict.get(callback)) })
#            else:
#                print(f"{callback} not in collections dictionary.")
#                continue
#        return callback_dict
#
#    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=get_callbacks_dict(str2list(load_config("data_chanels"), " "), db_name, collection_dict)  ))

### Funkce na generovani callbacks v tomto souboru, akorat predani v promenne  - NEFUNGUJE, CORE PROBLEM!!!
#    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=db_callbacks))

### Funkce get_callbacks_dict v samostatne knihovne, volani v ramci main funkce - FUNGUJE, FINALNI RESENI
    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=get_callbacks_dict(str2list(load_config("data_chanels"), " "), db_name, collection_dict)  ))

    f.run()


if __name__ == '__main__':
    
    if not validate_config("max_depth", "data_chanels", "pairs", "exchange", "db_name"):
        print("Some key config items are missing. EXITING")
        exit(1)

    db_name = load_config("db_name")
    collection_dict = {"TRADES" : "trades",
                "TICKER" : "ticker",
                "VOLUME" : "volume",
                "FUNDING" : "funding",
                "BOOK_DELTA" : "book_delta",
                "L2_BOOK" : "l2_book",
                "L3_BOOK" : "l3_book"}

    max_depth = load_config("max_depth", pre=int)
    data_chanels = get_obj_list(str2list(load_config("data_chanels"), " "), DATA_CHANELS)
    db_callbacks = get_callbacks_dict(str2list(load_config("data_chanels"), " "), db_name, collection_dict)
    
    pairs = str2list(load_config("pairs"), " ")
    exchange =  conf2obj(load_config("exchange"),  EXCHANGES)
    main(exchange, max_depth, data_chanels, pairs, db_callbacks)
#print(data_chanels.pop() == TRADES)

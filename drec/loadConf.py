CONF_FILE="default.conf"

def get_lines(file):
    with open(file) as f:
        lines = f.readlines()
        return lines

def get_conf_dict(conf_lines):
    conf_dict=dict()

    for line in conf_lines:
        try:
            key, value = line.split("=")
            if not key.strip()[0] == "#":
                conf_dict.update( {key.strip() : value.strip()} )
        except ValueError:
            continue
    
    return conf_dict

def load_config(config_item, pre=None, post=None):
    loaded_conf = None
    try:
        loaded_conf = conf_dict[config_item]
        if post:
            loaded_conf = post(loaded_conf)
        if pre:
            loaded_conf = pre(loaded_conf)
    except:
        print("Wrong or none configuration for:", config_item)
    
    return loaded_conf

def str2list(conf_string, separator):
    conf_list = conf_string.split(separator)

    return conf_list

def validate_config(*config_items):
    for item in config_items:
        if not load_config(item):
            return False
        else:
            continue
    
    return True

conf_dict = get_conf_dict(get_lines(CONF_FILE))